# Team-Capture Assets

This repo contains the source editing files for the custom made assets in [Team-Capture](https://tc.voltstro.dev).

These files are provided AS-IS! How well they go in source controll? Well lets find out.

# Links

- [Website](https://tc.voltstro.dev)
- [Source Code Repo](https://github.com/Voltstro-Studios/Team-Capture)
- [Assets Source Repo](https://gitlab.com/Voltstro-Studios/TC/Team-Capture-Assets)